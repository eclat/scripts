# ######################################################################## #
# Configuration file for ec2setup.                                         #
# Copy it to /etc/default/ec2setup and edit to your liking.                #
########################################################################## #

# ######################################################################## #
# Section 1: AWS configuration                                             #
#                                                                          #  
# These variables are optional.  They configure the availability  region   # 
# and provide additional command line arguments for eclat(1).  Access      #
# credentials should be configured in eclat.conf(5).                       #
# ######################################################################## #

# Sets the region where the instance is located.
EC2_REGION=us-east-1

# Additional options for eclat(1).
EC2_ECLAT_OPTIONS=

# ######################################################################## #
# Section 2: EC2 tags configuration                                        #
#                                                                          #
# Variables in this section declare names of EC2 tags that hold various    # 
# bits of instance-specific data.  All of these are optional.              #
# ######################################################################## #

# A tag containing the hostname.  Its value must be a fully-qualified domain 
# name for this instance.  It is used to configure /etc/hostname, 
# /etc/hosts, and to set up an A DNS record for that hostname, if dynamic 
# DNS is configured (see below).
EC2_SETUP_TAG_HOSTNAME=hostname

# A tag with the name of DNS zone for A records holding local EC2 
# addresses (those from the 10.0.0.0/8 network).  A record in this
# zone is formed by taking the first component of the hostname 
# as set by the $EC2_SETUP_TAG_HOSTNAME tag.
EC2_SETUP_LOCAL_ZONE=

# This tag, if defined, contains cnames for that host.  Its value must be 
# a whitespace-separated list of CNAME records for this hostname.  Each 
# CNAME must follow the usual BIND convention: if it ends with a dot, it is 
# taken verbatim, otherwise a dot and the hostname (as set by the 
# EC2_SETUP_TAG_HOSTNAME tag) are appended to it.
EC2_SETUP_TAG_CNAMES=cnames

# If set, this variable instructs ec2setup to form CNAME records from
# the ServerName and ServerAlias directives in /etc/apache2/sites-enabled/*
# files.  The value of this variable should be a whitespace-separated list
# of domain name patterns (in the sense of glob(3)).  Only those names that
# match one of these patterns will be selected for CNAMES.
#
# When both EC2_SETUP_TAG_CNAMES and EC2_SETUP_CNAMES_FROM_APACHE are used,
# the two lists are merged.
EC2_SETUP_CNAMES_FROM_APACHE=

# A tag describing that instance.  If defined and dynamic DNS is configured
# (see below) its value is used to set up a TXT record for this host.
EC2_SETUP_TAG_DESCR=Name

# A tag with the assigned elastic IP.  If set, its value will be used as IP 
# for that host.
EC2_SETUP_TAG_IP=ipaddr

# ######################################################################## #
# Section 3: Dynamic DNS configuration                                     #
#                                                                          #
# Variables in this session configure access to the dynamic DNS.           #
# ######################################################################## #

# IP address of the name server.  If you have several domains with different
# servers, use the following syntax:
#
#   "DOMAIN1=SERVER1 DOMAIN2=SERVER2"
#
# Each DOMAIN[X] should end with a dot.
#
# When looking for a match, ec2setup will continuously strip leading
# name components from the domain name, until the remaining string
# matches one of the domains in the EC2_SETUP_NSUPDATE_SERVER value.
# Thus, the entry .=SERVER will match any domain name.
EC2_SETUP_NSUPDATE_SERVER=
# Nmae of the key file, suitable as an argument to the -k option of
# nsupdate(1).  If you have several domains, use the same syntax as
# for EC2_SETUP_NSUPDATE_SERVER.
#EC2_SETUP_NSUPDATE_KEY=/etc/ec2setup/Kfoobar.+157+12345

# If this tag is specified, its value controls whether or not to update the
# DNS.  If the value is "yes", the DNS updates are enabled.  If it is "no",
# the updates are disabled.  Setting it to "no" is convenient when starting 
# a new instance from an image of another instance, to avoid clobbering 
# existing NS records.
EC2_SETUP_TAG_NSUPDATE=nsupdate

# ######################################################################## #
# Section 4: LDAP configuration                                            #
#                                                                          #
# The variables below instruct ec2setup to update LDAP entry for this host #
# each time it is started or shut down.                                    #   
#                                                                          #
# The LDAP setup assumes gray's schema.                                    #
# ######################################################################## #

# Options for ldapsearch and ldapmodify.  Normally you would specity at
# least the -D option (binddn) and -w or -y (recommended), as in the
# example below:
#EC2_SETUP_LDAP_OPTIONS="-D 'cn=ec2setup,dc=example,dc=net' -y /etc/ldap.pass"
# Base name:
#EC2_SETUP_LDAP_BASE="ou=ec2,dc=example,dc=net"

# Use this variable to insert static information to the LDAP record.  The 
# format is "attr: value".  Multiple attributes are allowed, each on a 
# separate line.
EC2_SETUP_STATIC_INFO=

# End of ec2setup configuration.

